#!/usr/bin/env python

import os, sys, re, shutil, argparse
import lsf_map

# General methods
# ------------------------------------------------------------------------------------------

def path_from_script_dir(subpath):
	# originally /broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/<subpath>
	return os.path.join(os.path.abspath(os.path.dirname(__file__)), subpath)

def soft_mkdir(mydir):
	if not os.path.isdir(mydir):
		os.mkdir(mydir)

def files_for_organism(organism):
	base_dir = path_from_script_dir("Reference/")
	if organism.lower() == 'mouse':
		refseq_file = os.path.join(base_dir, "Mouse_RefSeq/refMrna_ERCC_polyAstrip.mm10.fa")
		sym2ref_file = os.path.join(base_dir, "Mouse_RefSeq/refGene.mm10.sym2ref.dat")
		hom_file = os.path.join(base_dir, "mouse_to_human")
	elif organism.lower() == 'human':
		refseq_file = os.path.join(base_dir, "Human_RefSeq/refMrna_ERCC_polyAstrip.hg19.fa")
		sym2ref_file = os.path.join(base_dir, "Human_RefSeq/refGene.hg19.sym2ref.dat")
		hom_file = None
	else:
		raise Exception("Unknown species")

	return refseq_file, sym2ref_file, hom_file

def get_read_names(read_pairs_file):
	read_names = list()
	with open(read_pairs_file, 'r') as reads:
		for line in reads:
			fields = line.strip().split()
			r1_fastq, r2_fastq, idx_fastq = fields[0], fields[1], None
			if len(fields) > 2:
				idx_fastq = fields[2]
			read_names.append((r1_fastq, r2_fastq, idx_fastq))
	return read_names

# Command generation
# ------------------------------------------------------------------------------------------

def make_validate_cmd(read_pairs_file, barcodes_file, 
	samples_described_file, samples_compared_file, reads_dir):
	python = "python " + path_from_script_dir("validate_inputs.py")
	cmd = " ".join([python, read_pairs_file, barcodes_file, 
		samples_described_file, samples_compared_file, reads_dir])
	return cmd

def make_split_cmd(reads_dir, alignment_dir, barcode_file, r1_fastq, r2_fastq, idx_fastq):
	python = "python " + path_from_script_dir("new_split_samples.py")
	cmd = " ".join([python, reads_dir, alignment_dir, barcode_file, r1_fastq, r2_fastq])
	if idx_fastq is not None:
		cmd += " --idx_fastq "+idx_fastq
	return cmd

def make_bwa_cmd(fastq_path, reference_prefix, alignment_dir):
	# bwa = "/broad/software/free/Linux/redhat_5_x86_64/pkgs/bwa_0.7.4/bwa"
	bwa = "bwa"
	aln = "aln -l 24 %s %s" % (reference_prefix, fastq_path)
	samse = "samse %s - %s > %s.sam" % (reference_prefix, fastq_path, fastq_path)
	cmd = " ".join([bwa, aln, "|", bwa, samse])
	return cmd

def make_count_cmd(sym2ref_file, barcode_file, alignment_dir, counting_dir):
	python = "python " + path_from_script_dir("new_merge_and_count.py")
	cmd = " ".join([python, sym2ref_file, barcode_file, alignment_dir, counting_dir])
	return cmd

def make_rnk_cmd(edger_dir, rnk_dir, hom_file):
	python = "python " + path_from_script_dir("edger_to_rnk.py")
	cmd = " ".join([python, edger_dir, rnk_dir])
	if hom_file is not None:
		cmd += " --hom_file "+hom_file
	return cmd

def make_gsea_cmd(rank_file, gmt_file, out_dir):
	java = "java -cp"
	jar = path_from_script_dir("java/gsea2-2.1.0.jar")
	memreq = "-Xmx1G"
	module = "xtools.gsea.GseaPreranked"
	global_pars = " ".join(["-collapse false",
							"-set_min 15",
							"-set_max 500",
							"-scoring_scheme weighted",
							"-nperm 1000",
							"-norm meandiv",
							"-mode Max_probe",
							"-include_only_symbols true",
							"-rnd_seed timestamp"
						])
	input_pars = "-gmx %s -rnk %s" % (gmt_file, rank_file)
	output_file = "_".join([os.path.join(out_dir, os.path.basename(rank_file)).split(".rnk")[0], 
		os.path.basename(gmt_file), "output"])
	output_pars = "-out %s" % output_file 
	#log_pars = "> %s" % os.path.join(out_dir, "gsea.log")
	cmd = " ".join([java, jar, memreq, module, global_pars, input_pars, output_pars])
	return cmd

# Pipeline steps
# ------------------------------------------------------------------------------------------

def run_validate(read_pairs_file, barcodes_file, 
		samples_described_file, samples_compared_file, reads_dir):
	os.system(make_validate_cmd(read_pairs_file, barcodes_file, samples_described_file, 
		samples_compared_file, reads_dir))

def run_split(read_pairs_file, barcodes_file, reads_dir, alignment_dir):
	cmd_array = []
	for (r1_fastq, r2_fastq, idx_fastq) in get_read_names(read_pairs_file):
		cmd_array.append(make_split_cmd(reads_dir, alignment_dir, barcodes_file, 
			r1_fastq, r2_fastq, idx_fastq))
	lsf_map.execute(cmd_array, reads_dir, memreq="4000")

def run_bwa(reference_prefix, alignment_dir):
	cmd_array = []
	for fastq in filter(lambda x: x.endswith('.fastq'), os.listdir(alignment_dir)):
		fastq_path = os.path.join(alignment_dir,fastq)
		cmd_array.append(make_bwa_cmd(fastq_path, reference_prefix, alignment_dir))
	lsf_map.execute(cmd_array, alignment_dir, memreq="4000")

def run_count(sym2ref_file, barcode_file, alignment_dir, counting_dir):
	os.system(make_count_cmd(sym2ref_file, barcode_file, alignment_dir, counting_dir))

def run_count_qc():
	os.system("Rscript %s ." % path_from_script_dir("transform_counts.R"))
	os.system("Rscript %s ." % path_from_script_dir("qc_analysis.R"))

def run_edger(edger_dir, rnk_dir, hom_file):
	os.system("Rscript %s ." % path_from_script_dir("de_analysis.R"))
	os.system("Rscript %s ." % path_from_script_dir("de_plots.R"))
	os.system(make_rnk_cmd(edger_dir, rnk_dir, hom_file))

def run_gsea(comparisons_file, gmt_files, rnk_dir, gsea_dir):
	rnk_files = []
	with open(comparisons_file,'r') as comps:
		rnk_files  = [os.path.join(rnk_dir, comp.strip()+".rnk") for comp in comps.readlines()]
	cmd_array = []
	for rnk_file in rnk_files:
		for gmt_file in gmt_files:
			cmd_array.append(make_gsea_cmd(rnk_file, gmt_file, gsea_dir))
	lsf_map.execute(cmd_array, gsea_dir, memreq="1000")

# Main method
# ------------------------------------------------------------------------------------------

def main():

	# argparsing
	parser = argparse.ArgumentParser()
	parser.add_argument("--organism", help="mouse (default) OR human", default="mouse")
	parser.add_argument("--start", type=int, help="index of starting step", default=0)
	parser.add_argument("--end", type=int, help="index of ending step", default=99)
	args = parser.parse_args()

	org = args.organism
	steps = range(args.start, args.end+1)
	# can also run specific subset, e.g.
	# steps = [1,2,5]

	# directories
	reads_dir = "Reads"
	alignment_dir = "Alignment"
	counting_dir = "Counting"
	edger_dir = "EdgeR"
	gsea_dir = "GSEA"
	rnk_dir = "GSEA/Ranks"

	soft_mkdir(alignment_dir)
	soft_mkdir(counting_dir)
	soft_mkdir(edger_dir)
	soft_mkdir(gsea_dir)
	soft_mkdir(rnk_dir)

	# input files
	read_pairs = "read_pairs.txt"
	samples_described = "samples_described.txt"
	samples_compared = "samples_compared.txt"
	barcodes = "barcodes.txt"

	# organism-specific files
	refseq_file, sym2ref_file, hom_file = files_for_organism(org)
	
	# etc files
	gene_sets = ["c5.bp.gmt", "c5.cc.gmt", "c5.mf.gmt"]
	gmt_dir = path_from_script_dir("gmt/")
	gmt_files = [os.path.join(gmt_dir, g) for g in gene_sets]
	counts_file = os.path.join(counting_dir, "counts.umi.dat")

	# do stuff
	# TODO something less hacky?
	if 0 in steps:
		run_validate(read_pairs, barcodes, samples_described, samples_compared, reads_dir)
	if 1 in steps:
		run_split(read_pairs, barcodes, reads_dir, alignment_dir)
	if 2 in steps:
		run_bwa(refseq_file, alignment_dir)
	if 3 in steps:
		run_count(sym2ref_file, barcodes, alignment_dir, counting_dir)
	if 4 in steps:
		run_count_qc()
	if 5 in steps:
		run_edger(edger_dir, rnk_dir, hom_file)
	if 6 in steps:
		run_gsea(samples_compared, gmt_files, rnk_dir, gsea_dir)

if __name__ == '__main__':
	main()
