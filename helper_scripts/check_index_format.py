import os, sys, gzip, re

fastq_file = sys.argv[1]

def open_fastq_or_gz(filename):
	if filename.endswith(".fastq") and os.access(filename, os.F_OK):
		return open(filename, "rU")
	elif filename.endswith(".fastq.gz") and os.access(filename, os.F_OK):
		return gzip.open(filename, "rb")
	elif filename.endswith(".fastq") and os.access(filename + ".gz", os.F_OK):
		return gzip.open(filename + ".gz", "rb")
	elif filename.endswith(".fastq.gz") and os.access(filename[:-3], os.F_OK):
		return open(filename[:-3], "rU")
	raise IOError, "Unknown file: " + filename

sequence = re.compile(r"^[acgtnACGTN]+$")
numeric = re.compile(r"^[0-9]+$")

with open_fastq_or_gz(fastq_file) as fastq:
	header = fastq.readline()
	index = header.strip().split()[1].split(':')[-1]
	print "Sample index: "+index
	if len(index) == 0:
		print "Indexes are blank. You'll need to specify a separate " \
			+ "barcodes file in read_pairs.txt."
	elif re.search(numeric, index) is not None:
		print "Indexes are numeric. You'll need to replace the indexes " \
			+ "in barcodes.txt with the plate number."
	elif re.search(sequence, index) is not None:
		if len(index) == 8:
			print "Indexes are correctly included in this file."
		else:
			print "Indexes are included, but they're not 8bp long. " \
				+ "The pipeline cannot handle this."
	else:
		print "Indexes are in an unknown format. The pipeline cannot handle this."