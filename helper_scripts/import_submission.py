import os, sys

submission_file = sys.argv[1]

barcodes_file = 'barcodes.txt'
samples_described_file = 'samples_described.txt'

barcode_lines = list()
sample_lines = list()
with open(submission_file, 'r') as submission:
	for line in submission:
		line = line.strip()
		if len(line) == 0:
			 # add empty lines to samples file
			sample_lines.append(line)
		else:
			sample, plate, well, tag, index = line.split()
			plate_well = plate+'_'+well
			sample_lines.append('\t'.join([sample, plate_well]))
			barcode_lines.append('\t'.join([plate_well, tag, index]))

with open(barcodes_file, 'w') as barcodes:
	barcodes.write('\n'.join(barcode_lines))

with open(samples_described_file, 'w') as samples:
	samples.write('\n'.join(sample_lines))

print "Import successful!"