
=================================================================
==== DGE PIPELINE README ========================================
=================================================================


---- INPUT FILES ------------------------------------------------

First, you'll need to create a project directory, preferably with 
the date of the experiment and initials of the experimenter(s).
For real (non-testing) runs, put the directory in here:

/broad/xavierlab_datadeposit/DGE_rnaseq/

Inside that directory, you'll need a subdirectory called "Reads"
that contains all the necessary FASTQ files (gzipped files are fine).

You'll also need these files (all tab-separated):
read_pairs.txt -- list of read1, read2 FASTQ pairs
barcodes.txt -- maps plate/well locations to tag and index sequences
samples_described.txt -- maps condition names to plate/well locations
samples_compared.txt -- list of comparisons to analyze with GSEA

See the example data to get a sense of the exact formatting.

These files can easily be created using the sample submission spreadsheet.
You can create barcodes.txt and samples_described.txt automatically by
copying the (Sample, Plate, Well, Tag, Index) columns of the spreadsheet
into a file and running import_submission.py.

NOTE: if you're working with standard HiSeq data, you might have separate
index/barcode files. If so, then you need to add an extra column to 
read_pairs.txt with the index file corresponding to each read pair.


---- RUNNING THE PIPELINE ----------------------------------------

1. Make sure that your input files are all set up.

2. Import the following dotkits:
$ reuse -q LSF
$ reuse -q Python-2.7
$ reuse -q R-3.0
$ reuse -q BWA

3. Go to the base directory of your project and run:
$ nohup /broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/super_dge_pipeline.py &

Call the script with --help to learn about optional arguments.


---- MONITORING THE PIPELINE ----------------------------------------

There are various ways to check pipeline progress:

1. Using "top": 
-Counting will show up as a python process.
-EdgeR will show up as an R process. 

2. Using "bjobs": 
-Splitting will spawn a bunch of nodes running python.
-Alignment will spawn a bunch of nodes running bwa.
-GSEA will spawn a bunch of nodes running java.

3. Checking what files have been created in your project directory.

If the pipeline dies prematurely, logging output will appear in nohup.out (or whatever
log file you piped the command to). That should give you a sense of what went wrong.
Note that the default log will append to (rather than overwrite) nohup.out, so make 
sure you're looking at the right stack trace!


---- SYSTEM REQUIREMENTS ------------------------------------------------

This needs to be run on the Broad filesystem, since it relies on dotkits, LSF and such.

The counting step will occasionally take a bunch of time and memory. This may 
have to do with I/O limitations of the Broad's distributed filesystem. It 
might take especially long if the host server (e.g. boltzmann) is low on 
available memory.


---- EXAMPLE DATA ------------------------------------------------------------

An example of a project directory (with all necessary inputs) before the 
pipeline has been run:

/broad/xavierlab_datadeposit/rnaseq/DGE/test_data/

The same project after running the pipeline:

/broad/xavierlab_datadeposit/rnaseq/DGE/test_results/


---- HELPER SCRIPTS ------------------------------------------------------------

Various external scripts are located in:

/broad/xavierlab_datadeposit/rnaseq/DGE/DGE_pipeline/Scripts/helper_scripts/

Of particular note:

check_index_format.py -- checks the index format in an input .fastq or .fastq.gz file
import_submission.py -- converts columns from the submission spreadsheet into barcodes.txt
	and samples_described.txt files


